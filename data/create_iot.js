iot = db.getSiblingDB("iot")

iot.createUser(
  {
    user: "iotuser",
    pwd: "iot123",
    roles: [ { role: "readWrite", db: "iot" } ]
  }
)