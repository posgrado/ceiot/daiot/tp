cfg = {
    _id: "rs",
    members: [
        {
            _id: 0,
            host: "db_primary:27017",
            priority: 2
        },
        {
            _id: 1,
            host: "db_secondary:27017",
            priority: 1
        },
        {
            _id: 2,
            host: "db_arbiter:27017",
            arbiterOnly: true,
            priority: 0
        }
    ]
}
rs.initiate(cfg)
rs.status()
