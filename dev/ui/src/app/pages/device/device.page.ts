import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DevicesService } from 'src/app/services/devices.service';
import { Device } from '../../model/device';
import { lastTele } from '../../model/lastTele';
import { IonDatetime, ModalController, ViewDidEnter, ViewWillEnter } from '@ionic/angular';
import * as Highcharts from 'highcharts';
import { Messure } from 'src/app/model/messure';
import { MessureService } from '../../services/messure.service';
import { CustomDatePipe } from 'src/app/pipe/datetime.pipe';
import { SprayService } from '../../services/spray.service';
import { Spray } from 'src/app/model/spray';
import { ModalInfoPage } from '../modal-info/modal-info.page';
import { Tab1PageModule } from '../../tab1/tab1.module';
declare var require: any;
require('highcharts/highcharts-more')(Highcharts);
require('highcharts/modules/solid-gauge')(Highcharts);

@Component({
  selector: 'app-device',
  templateUrl: './device.page.html',
  styleUrls: ['./device.page.scss'],
  providers:[CustomDatePipe]
})
export class DevicePage implements OnInit, ViewWillEnter {

  private valorObtenido:number=0;//valor del sensor
  public myChart;
  private chartOptions;
  private device : Device;//dispositivo 
  private messure : Messure;//ultima medicion
  private sprayArray : Spray[];//arreglo de logs de riego
  private allMessureArray : Messure[];//todas las mediciones de un sensor
  isOpen : boolean;//indica si e la valvula esta abierta o cerrada
  private lastTelemetry:lastTele;
  public deviceName:String;
  public humedadActual:number;
  public temperaturaActual:number;
  public timestamp:number;


  constructor(private router: ActivatedRoute, 
    private devService: DevicesService, 
    private messureService: MessureService,
    private myCustomDatePipe: CustomDatePipe,
    private sprayService: SprayService,
    private modalCtrl: ModalController) { 

  }

  

  ngOnInit() {
    console.log('ngOnInit');
    this.deviceName = this.router.snapshot.paramMap.get('id');
    console.log('Nombre Dispositivo: ' + this.deviceName); 
    this.getLast(this.deviceName);

  }

  /**
   * Obtiene el dispositivo llamando a la API a traves del id
   * @param id Id del dispositivo encuentado
   */
  async getLast(nombre){
     console.log('Se ejecuta getDeviceById'); 
     this.lastTelemetry = await this.devService.getLastTelemetry(nombre);
     console.log(this.lastTelemetry.telemetry[this.lastTelemetry.telemetry.length-1]);
     this.humedadActual=this.lastTelemetry.telemetry[this.lastTelemetry.telemetry.length-1].Humedad;
     this.temperaturaActual=this.lastTelemetry.telemetry[this.lastTelemetry.telemetry.length-1].Temperatura;
     this.timestamp=this.lastTelemetry.telemetry[this.lastTelemetry.telemetry.length-1].ts;
    
   }

  /**
   * Obtiene la ultima medicion del dispositivo llamando a la API a traves del id
   * @param id Id del dispositivo encuentado
   */
  // async getMessureById(id){
  //   console.log('Se ejecuta getMessureById'); 
  //   this.messure = await this.messureService.getLastMessure(id);
  //   console.log(this.messure);    
  // }

  /**
   * Obtiene todos los logs de riego del dispositivo llamando a la API a traves del id
   * @param id Id del dispositivo encuentado
   */
  // async getSprayById(id){
  //   console.log('Se ejecuta getSprayById')
  //   this.sprayArray = await this.sprayService.getAllSpray(id);
  //   console.log(this.sprayArray);
  // }

  /**
   * Obtiene todas los mediciones del dispositivo llamando a la API a traves del id
   * @param id Id del dispositivo encuentado
   */
  // async getAllMessureById(id){
  //   console.log('Se ejecuta getAllMessureById');
  //   this.allMessureArray = await this.messureService.getAllMessure(id);
  //   console.log(this.allMessureArray);
  // }

  /**
   * Evento que actualiza en pantalla el chart 
   */
  ionViewDidEnter() {
    console.log('ionViewDidEnter');
  
  }


  ionViewWillEnter(): void {
    console.log('ionViewWillEnter');
  }









}
