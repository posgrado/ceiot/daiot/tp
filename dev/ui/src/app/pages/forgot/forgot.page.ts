import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../services/authentication.service';
import { AlertController, LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-forgot',
  templateUrl: './forgot.page.html',
  styleUrls: ['./forgot.page.scss'],
})
export class ForgotPage implements OnInit {
  credentials: FormGroup;
  submitted = false;

  constructor(
    private fb:FormBuilder,
    private authService: AuthenticationService,
    private alertController: AlertController,
    private router: Router,
    private loadingController: LoadingController) { }

  ngOnInit() {
    this.credentials= this.fb.group({
      email: ['', [Validators.required, Validators.email]],
    });    
  }

  /**
   * Evento del boton submit
   * @returns 
   */
  onSubmit(): void {
    this.submitted = true;
    if(this.credentials.invalid){
      console.log("Error en submit");
      return;
    }
    //console.log(JSON.stringify(this.formRegister.value, null, 2));
    this.forgot();
  }

  /**
   * Resetea los campos del formulario
   */
  onReset(): void {
    this.submitted=false;
    this.credentials.reset();
  }


  /**
   * HAce el proceso de registrar un nuevo usuario
   */
  async forgot(){
    try{
      const loading = await this.loadingController.create();
      await loading.present();
      this.authService.forgot(this.credentials.value).subscribe(
        async (res) => {
          await loading.dismiss();
          this.router.navigateByUrl('/login', {replaceUrl:true});
        },
        async (res) => {
          await loading.dismiss();
          const alert = await this.alertController.create({
            header: 'Recovering failed',
            message: res.error.error,
            buttons: ['OK'],
          });
          await alert.present();
        }
      )
    }
    catch(error){
      console.log('Error->', error);
    }
  }

  get f(): {[key:string]: AbstractControl}{
    return this.credentials.controls;
  }

}
