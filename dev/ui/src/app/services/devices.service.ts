
import { Injectable } from '@angular/core';
import { CookiesService } from '../services/cookies.service';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Device } from '../model/device';
import { lastTele } from '../model/lastTele';
import { environment } from 'src/environments/environment';

export const TOKEN_KEY = 'my-token';

@Injectable({
  providedIn: 'root'
})
export class DevicesService {

  urlApi = `http://localhost:3000`

  constructor(private _http: HttpClient, private cookies:CookiesService) { }

  /**
   * Metodo que llama a la api para recuperar todos los dispositivos de la base de datos
   * @returns Devuelve el listado de dispositivos almacenados
   */
  getDevices(): Promise<Object[]> {
    const token = this.cookies.getDataFromCookies(TOKEN_KEY);
    const headers = new HttpHeaders().set('Authorization', token);
    return this._http.get(this.urlApi + "/dispo/all", { headers: headers })
      .toPromise()//devuelve una promesa
      .then((list: Object[]) => {//cuando se tiene el valor en el then se castea y se convierte al tipo que se necesita devolver
        return list;
      });
  }


    /**
   * Metodo que llama a la api para recuperar todos los dispositivos de la base de datos
   * @returns Devuelve el listado de dispositivos almacenados
   */
     getLastTelemetry(nameDispo:string): Promise<lastTele> {
      const token = this.cookies.getDataFromCookies(TOKEN_KEY);
      const headers = new HttpHeaders().set('Authorization', token);
      return this._http.get(this.urlApi + "/last/"+nameDispo, { headers: headers })
        .toPromise()//devuelve una promesa
        .then((list: lastTele) => {//cuando se tiene el valor en el then se castea y se convierte al tipo que se necesita devolver
          console.log(list);
          return list;
        });
    }

  /**
   * Metodo que llama a la api para recuperar un dispositivo de la base de datos
   * @param id Id del dispositivo a recuperar
   * @returns Devuelve el listado de dispositivos almacenados
   */
  getDevice(id): Promise<Device> {
    console.log('Se ejecuta promesa getDevice');
    return this._http.get(this.urlApi + "/api/device/" + id)
      .toPromise()//devuelve una promesa
      .then((device: Device) => {//cuando se tiene el valor en el then se castea y se convierte al tipo que se necesita devolver
        console.log('Termina ejecucion exitosa promesa getDevice');
        return device;
      });

  }

}
