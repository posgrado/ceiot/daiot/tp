import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, from } from 'rxjs';
import { CookiesService } from '../services/cookies.service';
import { environment } from 'src/environments/environment';

export const TOKEN_KEY = 'my-token';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  isAuthenticated: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(null);
  token = '';

  constructor(private http:HttpClient, private cookies:CookiesService) { 
    this.loadToken();
  }

  /**
   * Recupera el token en caso de que haya sido guardado en las cookies y asi evitar loggearse nuevamente
   */
  async loadToken(){
    try{
      const token = this.cookies.getDataFromCookies(TOKEN_KEY);
      if (token){
        this.token = token;
        this.isAuthenticated.next(true);
      }
      else{
        this.isAuthenticated.next(false);
      }
    }
    catch(error){
      console.log('Error->', error);
    }
  }

  /**
   * Ejecuta el proceso de login 
   * @param credentials Email y password ingresado
   * @returns 
   */
  login(credentials: {email, password}): Observable<any>{
    try{
      this.isAuthenticated.next(true);
      console.log(credentials);
      console.log(`${environment.backend_api_url}/login`);
      return this.http.post(`${environment.backend_api_url}/login`, credentials);
      
    }
    catch(error){
      console.log('Error->', error);
    }
  }

  /**
   * Llama a la api para crear un nuevo usuario
   * @param credentials Datos del usuario a registrarse
   * @returns 
   */
  register(credentials: {name, email, password, acceptTerms, rol}): Observable<any>{
    try{
      return this.http.post(`${environment.backend_api_url}/register`, credentials);
    }
    catch(error){
      console.log('Error->', error);
    }
  }

  forgot(credentials: {email}): Observable<any>{
    try{
      return this.http.post(`${environment.backend_api_url}/forgotpassword`, credentials);
    }
    catch(error){
      console.log('Error->', error);
    }
  }

  /**
   * Proceso de logout que resetea las cookies para que la proxima vez me pida logueo
   */
  logout() {
    this.isAuthenticated.next(false);
    this.cookies.deleteDataFromCookies(TOKEN_KEY);
  }
}
