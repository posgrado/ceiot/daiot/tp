
#include "mqtt.h"
#include "certs.h"


//--Objetos externos
extern WiFiClientSecure espClient;
extern PubSubClient mqttClient;
extern TimerHandle_t mqttReconnectTimer;
extern String mqtt_server;
extern String mqtt_tcp_str;

//--Variables externas
extern String topic_act;
extern String device;

//--Variables locales
String topic_rec;
String msg_rec;
const char* command;
const char* param;
const char* dev_rec;
uint8_t canal_rec;
uint8_t accion_rec;
uint8_t TRYS_LOAD_FILES=3;

/*void load_client_key(void){
  File client_key = SPIFFS.open("/client.key", "r");
  if (!client_key) {
    Serial.println("Fallo apertura del certificado");
  }
  else{
    Serial.println("Certificado abierto");
  }
  delay(300);
  if (espClient.loadPrivateKey(client_key,sizeof(client_key))){
    Serial.println("Certificado cargado");
  }
  else{
    Serial.println("Certificado NO cargado");
  }
  
}*/


void mqtt_init(void){
  espClient.setCACert(ca_cert);
  espClient.setCertificate(client_crt);
  //load_client_key();
  espClient.setPrivateKey(client_key);
  mqttClient.setServer(mqtt_server.c_str(), mqtt_tcp_str.toInt());//mqttClient.setServer("192.168.1.42", 8883);
  mqttClient.setCallback(receivedCallback);
}

void mqtt_connect() {
  /* Loop until reconnected */
  while (!mqttClient.connected()) {
    Serial.print("Conectando a MQTT...");
    /* connect now */
    if (mqttClient.connect(device.c_str())){
      Serial.println("Conectado");
      /* subscribe topic */
      mqttClient.subscribe(topic_act.c_str());
    } else {
      Serial.print("falla, codigo status =");
      Serial.print(mqttClient.state());
      Serial.println("Nuevo intento en 5 segundos");
      /* Wait 5 seconds before retrying */
      delay(5000);
    }
  }
}


void receivedCallback(char* topic, byte* payload, unsigned int length) {
  Serial.println("Publicacion recibida.");
  payload[length] = '\0';
  topic_rec = String((char*)topic);
  msg_rec = String((char*)payload);
  Serial.print("Topic: ");
  Serial.println(topic_rec);
  Serial.print("Payload: ");
  Serial.println(msg_rec);

  StaticJsonDocument<200> parse_payload;
  DeserializationError error = deserializeJson(parse_payload, msg_rec);
  if (error) {
    Serial.print("[DEBUG] deserializeJson() fallo: ");
    Serial.println(error.f_str());
    return;
  }

  //Modelo de entrada: {"Device":"DAIoT01","Command":"on", "Parameter":"1"} 
  dev_rec=parse_payload["Device"];
  command=parse_payload["Command"];
  if (parse_payload.containsKey("Parameter")){
    param=parse_payload["Parameter"];
  }else{
    param="";
  }

  //--Debug del parseo de payload entrante
  //Serial.println("Comando:");
  //Serial.println(dev_rec);
  //Serial.println(command);
  //Serial.println(param);

  //--Si el comando es para este dispositivo, procesa
  if ((String)dev_rec==device){
    command_proc(command, param);
  }
}