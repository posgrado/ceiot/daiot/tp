def convertToC(certs_path='certs', fileName='include/certs.h'):
    cFile = open(fileName, "w+")
    ca_crt = open(f'{certs_path}/ca.crt', "r+")
    client_crt = open(f'{certs_path}/client.crt', "r+")
    client_key = open(f'{certs_path}/client.key', "r+")
    ca_crt_lines = ca_crt.read().splitlines()
    client_crt_lines = client_crt.read().splitlines()
    client_key_lines = client_key.read().splitlines()
    cFile.write("const char* ca_cert=\\\n")
    for line in ca_crt_lines[:-1]:
        cFile.write(f'"{line}\\n" \\\n')

    cFile.write(f'"{ca_crt_lines[-1]}\\n";')

    cFile.write(f'\n\n')

    cFile.write("const char* client_crt=\\\n")
    for line in client_crt_lines[:-1]:
        cFile.write(f'"{line}\\n" \\\n')

    cFile.write(f'"{client_crt_lines[-1]}\\n";')
    
    cFile.write(f'\n\n')

    cFile.write("const char* client_key=\\\n")
    for line in client_key_lines[:-1]:
        cFile.write(f'"{line}\\n" \\\n')

    cFile.write(f'"{client_key_lines[-1]}\\n";')

convertToC()
