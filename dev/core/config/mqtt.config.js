const fs = require('fs');

const mqttConf = {
    host: `mqtts://${process.env.DYNAMIC_ENVIRONMENT_URL}`,
    reconnectPeriod: 2000,
    commandTopic: 'device/command',
    attributeTopic: 'device/attribute',
    telemetryTopic: 'device/telemetry',
    key: fs.readFileSync('./certs/client.key'),
    cert: fs.readFileSync('./certs/client.crt'),
    ca: fs.readFileSync('./certs/ca.crt'),
}

module.exports = mqttConf;