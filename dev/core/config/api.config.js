const apiConf = {
    url: 'backend_api',
    port: 3000,
    method: 'PUT',
    commandRoute: '/command',
    telemetryRoute: '/telemetry',
    attributeRoute: '/attribute',
    actionRoute: '/action'
}

module.exports = apiConf;