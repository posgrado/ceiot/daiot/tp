
const dbConf = {
    mongoServerUrl_1 : 'db_primary',
    mongoPort_1 : 27017,
    mongoServerUrl_2 : 'db_secondary',
    mongoPort_2 : 27017,
    databaseName : 'iot',
    databaseUser : 'iotuser',
    databasePassword : 'iot123'
};

module.exports = {

    url: `mongodb://${dbConf.databaseUser}:${dbConf.databasePassword}@${dbConf.mongoServerUrl_1}:${dbConf.mongoPort_1},${dbConf.mongoServerUrl_2}:${dbConf.mongoPort_2}/${dbConf.databaseName}?replicaSet=rs&authSource=iot`, 
    dataNsamples:144
}