### The Ansible inventory file
resource "local_file" "AnsibleInventory" {
  content = templatefile("../configuration/inventory.tmpl",
    {
      bastion-dns = aws_eip.one.public_dns,
      bastion-ip  = aws_eip.one.public_ip,
      bastion-id  = aws_eip.one.id
    }
  )
  filename = "../configuration/inventory"
}

### Dynamic URL from AWS instance
resource "local_file" "deployURL" {
  content = templatefile("deploy.env.tmpl",
    {
      bastion-ip = aws_eip.one.public_ip
    }
  )
  filename = "../../deploy.env"
}

### Backend API URL from AWS instance
resource "local_file" "ionic_env_file" {
  content = templatefile("environment.ts.tmpl",
    {
      bastion-ip = aws_eip.one.public_ip
    }
  )
  filename = "../../dev/ui/src/environments/environment.prod.ts"
}
